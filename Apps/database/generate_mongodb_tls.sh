#!/bin/bash

FILES_DIR=roles/mongos/files

openssl req -newkey rsa:2048 -new -x509 -days 365 -nodes -out $FILES_DIR/mongodb.crt -keyout $FILES_DIR/mongodb.key
cat $FILES_DIR/mongodb.key $FILES_DIR/mongodb.crt > $FILES_DIR/mongodb.pem
