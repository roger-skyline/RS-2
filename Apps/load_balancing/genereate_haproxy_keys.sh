#!/bin/bash

FILES_DIR=roles/haproxy/files

mkdir -p $FILES_DIR

# Generate the .key file
openssl genrsa -out $FILES_DIR/haproxy.key 1024

# Generate the .csr file
openssl req -new -key $FILES_DIR/haproxy.key -out $FILES_DIR/haproxy.csr

openssl x509 -req -days 365 -in $FILES_DIR/haproxy.csr \
                    -signkey $FILES_DIR/haproxy.key \
                    -out $FILES_DIR/haproxy.crt

cat $FILES_DIR/haproxy.crt $FILES_DIR/haproxy.key | tee $FILES_DIR/haproxy.pem
