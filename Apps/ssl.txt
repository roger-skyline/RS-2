- CA generation
openssl genrsa -out CA.key 2048
openssl req -x509 -new -nodes -key CA.key -sha256 -days 1024 -out CA.pem

- generate certificate.key
openssl genrsa -out certificate.key 4096

- generate CSR for each host that will connectto the database
WARNING: the CN (Common Name) when setting the certificate must be the hostname of the host

openssl req -new -key certificate.key -out srv-db-master.csr # with CN srv-db-master
openssl req -new -key certificate.key -out srv-db-slave.csr # with CN srv-db-slave
openssl req -new -key certificate.key -out srv-preprod.csr # with CN srv-preprod
openssl req -new -key certificate.key -out srv-mgnt.csr # zith CN srv-mgnt
openssl req -new -key certificate.key -out srv-worker-1.csr # zith CN srv-worker-1
openssl req -new -key certificate.key -out srv-worker-2.csr # with CN srv-worker-2

- Wrap each csr file zith the CA root file of the host, in order to obtain a crt file

openssl x509 -req -in srv-db-master.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out srv-db-master.crt -days 500 -sha256
openssl x509 -req -in srv-db-slave.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out srv-db-slave.crt -days 500 -sha256
openssl x509 -req -in srv-preprod.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out srv-preprod.crt -days 500 -sha256
openssl x509 -req -in srv-mgnt.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out srv-mgnt.crt -days 500 -sha256
openssl x509 -req -in srv-worker-1.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out srv-worker-1.crt -days 500 -sha256
openssl x509 -req -in srv-worker-2.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out srv-worker-2.crt -days 500 -sha256

- Create the pem file for each host

cat certificate.key srv-db-master.crt > srv-db-master.pem
cat certificate.key srv-db-slave.crt > srv-db-slave.pem
cat certificate.key srv-preprod.crt > srv-preprod.pem
cat certificate.key srv-mgnt.crt > srv-mgnt.pem
cat certificate.key srv-worker-1.crt > srv-worker-1.pem
cat certificate.key srv-worker-2.crt > srv-worker-2.pem
