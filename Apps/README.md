## Apps
### Prod
- Install 2 separated workers
- Install a master DB and a slave
- Active SSL on server
- Install a loadbalancer for the 2 workers (redrobin needed for loadbalancing type)

### Preprod
- Install a worker and a SQL server
- Create scripts for deploy preprod config to prod


## About workers
Gonna use docker API to deploy containers on remote servers and watch them.
The goal is to deploy registered server and redeploy when a container is down.

## About database
MongoDB is chosen. It is way more interesting than SQL, mongoDB is largely used today,
and provides replica set tools perfect for the project.
