# Tasks group

Hostname
------
srv-1  : srv-nat.slash16.local   
srv-2  : srv-dhcp.slash16.local   
srv-3  : srv-mgnt.slash16.local   
srv-4  : srv-worker-1.slash16.local   
srv-5  : srv-worker-2.slash16.local   
srv-6  : srv-db-master.slash16.local   
srv-7  : srv-db-slave.slash16.local   
srv-8  : srv-preprod.slash16.local   
srv-9  : mail.slash16.local   
srv-10 : srv-services.slash16.local   

Core Structure (2VM)
------
### srv 1
- nat (port forwarding, ..)

### srv 2
- dns
- dhcp
- ldap

Global config (All)
------
- security (iptables, ssh, users, ...)

Monitoring + Maintaining(1 VM)a
------
### srv 3
- general automatisation
- backup (etc, worker, config, home ...)
- monitoring
- syslog 

Apps Prod + preprod (5 VM)
------
### srv 1
- loadbalancing (separate)
### srv 4
- worker 1
### srv 5
- worker 2
### srv 6
- database 1
### srv 7
- database 2
### srv 8 
- worker preprod
- database preprod

Services + Bonus (2 VM)
------
### srv 9
- mail

### srv 10
- versionning (gitlab)
- ftp
- xmpp

## Information :
Each steps must be automatised with bash script and deploy every config needed on all VM (if needed)
Script must have CLI arguments for several uses, like help and -f (config file)
