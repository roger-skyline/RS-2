## Monitoring + Maintainment
### Backup
- Install backup system of create it (S3, nfs ...)
- Chose what to backup (config file, home directory), make it configurable
- Perhabs use multiples VM to bakcup (only 8gb disks)

Bonus : config file versionning (etckeeper)

### Monitoring
- Chose an app, like shinken or nagios
- Install and configure it, config file must be saved periodically
- Configure a rsyslog server and all others VMs must be there client
- Deploy rsyslog client configuration on every VM

Bonus:	Install and configure a database system for metric informations
	Install and configure a data viewer for metric informations
