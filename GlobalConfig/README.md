## Global Config
### Security
- Configure NAT's iptables with port forwarding rules
- Deploy iptables file on every VM, with custom rules for every services
- Configure SSH access, config for every VM
- Create user account with different access on every VM
- Deploy all user ssh keys on every VM

### General automatisation
- Get all automatisations scripts from every service and deploy them to thoses concerned VM
- Handle debug mode and multiple argument, like --only-service

## Steps
### Initialisation
- Install Ansible on client machine
- Deploy ssh key from client to all ansible "slave"
- Deploy all basic configuration to "slave" (bashrc, iptables, ssh_config, users ...)
- Update all "slave"
- Install specific packages on each "slave"
- Deploy specific configurations on each
- Restart all services update on each
- Perform unitary test on each and report all result

Bonus:	If every VMs are erases, only one script must re-install every services / VM

### On gate VM
- Insert ssh key to gateway

## NAT steps
- ensure port forwarding is enable
- Enable custom iptables rules file

## Port forwarding step
- includes vars file for each service and each vm
- execute iptables rules
