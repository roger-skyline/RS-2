## All services must use a SSL certificate
A certification authority must create / revoke all certificates the infrastructure will use.

Services
------

- VM 1
  - haproxy.slash16.local
  - app.slash16.local

- VM 2
  - ldap.slash16.local
  
- VM 3
  - shinken.slash16.local
  - ara.slash16.local
  
- VM 4
  - worker1.slash16.local
  
- VM 5
  - worker2.slash16.local
  
- VM 6
  - mongo-master.slash16.local
 
- VM 7
  - mongo-slave.slash16.local

- VM 8
  - preprod-worker.slash16.local
  - preprod-db.slash16.local
  
- VM 9
  - mail.slash16.local
  
- VM 10
  - git.slash16.local
