##Classic update
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade


## install dnsutils for `dig`
sudo apt-get install --no-install-recommends dnsutils


## Create a self-signed certificate
sudo openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -out /etc/ssl/certs/mailcert.pem -keyout /etc/ssl/private/mail.key
#settings : Common Name (e.g. server FQDN or YOUR name) []:mail.domain
 
## Create a Certificate Signing Request (CSR).
sudo openssl req -nodes -days 3650 -newkey rsa:4096 -keyout /etc/ssl/private/mail.key -out mailcert.csr

sudo apt-get install dovecot-common dovecot-imapd
sudo apt-get install postfix
##settings : site internet THEN mail.domain



## modify config files
# see postfix_dovecot.patch



