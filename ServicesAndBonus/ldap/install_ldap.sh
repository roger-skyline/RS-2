

##add groups : 

###ou.ldif :
dn: ou=Groups,dc=rg-4,dc=cf
ou: Groups
objectClass: top
objectClass: organizationalUnit

dn: ou=People,dc=rg-4,dc=cf
ou: People
objectClass: top
objectClass: organizationalUnit


## apply
sudo ldapadd -cxWD cn=admin,dc=rg-4,dc=cf -f schema/ou.ldif

##add users

###user.ldif :
# UPG usertest

dn: cn=usertest,ou=Groups,dc=rg-4,dc=cf
cn: usertest
gidNumber: 5042
objectclass: top
objectclass: posixGroup

# User arc815
dn: uid=usertest,ou=People,dc=rg-4,dc=cf
cn: User Test
givenName: User
sn: test
uid: usertest
uidNumber: 5042
gidNumber: 5042
homeDirectory: /home/ldaphome
loginShell: /bin/bash
mail: usertest@mail.rg-4.cf
objectClass: top
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
userPassword: {SSHA}x

## apply:
sudo ldapadd -cxWD cn=admin,dc=rg-4,dc=cf -f schema/usertest.ldif

###create passwd for user:
sudo ldappasswd -xWD "cn=admin,dc=rg-4,dc=cf" -S "uid=usertest,ou=people,dc=rg-4,dc=cf"

## check all entry :
sudo slapcat


### install client :
sudo apt -y install libnss-ldap libpam-ldap ldap-utils
