#!/bin/sh

GRE='\033[0;32m'   # Green
RED='\033[0;31m'   # Red
NC='\033[0m'       # No Color

BACKUP_FOLDER=/var/server.backups/
BACKUP_FILE=$BACKUP_FOLDER"srv-ldap-backup.tar"
LOG_FILE=$BACKUP_FOLDER"srv-ldap-backup.log"
LDAP_BACKUP=ldap-backup.ldif
TAR_BACKUP="$BACKUP_FOLDER$LDAP_BACKUP /etc /root/ca" 

echo "====START BACKUP====" >> $LOG_FILE
echo $(date) >> $LOG_FILE

checkRet () {
  if [ $? -ne 0 ]
	 then
		echo "There is a problem on $1 for $2" | mail -s "[BACKUP] ERROR"  jclerc@mail.slash16.local
		printf "%s %21s ${RED}%21s${NC}\n" "$1" "$2" "KO" >> $LOG_FILE
		printf "%s %21s ${RED}%21s${NC}\n" "$1" "$2" "KO"
	else
		printf "%s %21s ${GRE}%21s${NC}\n" "$1" "$2" "OK" >> $LOG_FILE
		printf "%s %21s ${GRE}%21s${NC}\n" "$1" "$2" "OK"
  fi
}

SERVER='SRV-DHCP'
ACTION='BACKUP_LDAP_DB'
/usr/sbin/slapcat -b "dc=slash16,dc=local" -l $BACKUP_FOLDER$LDAP_BACKUP
checkRet $SERVER $ACTION

ACTION='CREATE_TAR'
tar cf $BACKUP_FILE $TAR_BACKUP  > /dev/null 2>&1
checkRet $SERVER $ACTION

ACTION='DELETE_LDAP_BACKUP'
rm $BACKUP_FOLDER$LDAP_BACKUP
checkRet $SERVER $ACTION

ACTION='CHOWN_TAR'
chown ansible:ansible $BACKUP_FILE
checkRet $SERVER $ACTION

echo "====STOP BACKUP====" >> $LOG_FILE
echo "======================================" >> $LOG_FILE
echo "======================================" >> $LOG_FILE

exit 0

