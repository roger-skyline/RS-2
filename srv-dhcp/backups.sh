#!/bin/sh

GRE='\033[0;32m'   # Green
RED='\033[0;31m'   # Red
NC='\033[0m'       # No Color

BACKUP_FOLDER=/var/servers.backups/
BORG_BACKUP_FOLDER=/var/borg.backup/
LOG_FILE=$BORG_BACKUP_FOLDER"borgsbackup.log"

echo "====START BACKUP====" >> $LOG_FILE
echo $(date) >> $LOG_FILE

checkRet () {
	if [ $? -ne 0 ]
	then
		echo "There is a problem on $1 for $2" | mail -s "backup" jclerc@mail.slash16.local
		echo "There is a problem on $1 for $2" | mail -s "backup" bbichero@mail.slash16.local
		printf "%15s %21s ${RED}%21s${NC}\n" "$1" "$2" "KO" >> $LOG_FILE
		printf "%15s %21s ${RED}%21s${NC}\n" "$1" "$2" "KO"
	else
		printf "%15s %21s ${GRE}%21s${NC}\n" "$1" "$2" "OK" >> $LOG_FILE
		printf "%15s %21s ${GRE}%21s${NC}\n" "$1" "$2" "OK"
	fi
}

# SSHFS
SERVER='SRV-NAT'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 -p 4284 \
	ansible@srv-nat.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-nat"
checkRet $SERVER $ACTION
SERVER='SRV-DHCP'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@srv-dhcp.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-dhcp"
checkRet $SERVER $ACTION
SERVER='SRV-MGNT'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@srv-mgnt.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-mgnt"
checkRet $SERVER $ACTION
SERVER='SRV-WORKER-1'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@srv-worker-1.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-worker-1"
checkRet $SERVER $ACTION
SERVER='SRV-WORKER-2'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@srv-worker-2.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-worker-2"
checkRet $SERVER $ACTION
SERVER='SRV-DB-MASTER'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@srv-db-master.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-db-master"
checkRet $SERVER $ACTION
SERVER='SRV-DB-SLAVE'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@srv-db-slave.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-db-slave"
checkRet $SERVER $ACTION
SERVER='SRV-PREPROD'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@srv-preprod.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-preprod"
checkRet $SERVER $ACTION
SERVER='SRV-MAIL'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@mail.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-mail"
checkRet $SERVER $ACTION
SERVER='SRV-SERVICES'
ACTION='MOUNT_SSHFS'
sshfs -o IdentityFile=/home/ansible/.ssh/id_ansible-slash16 \
	ansible@srv-services.slash16.local:/var/server.backups $BACKUP_FOLDER"srv-services"
checkRet $SERVER $ACTION

# BORG

SERVER='ALL_SERVERS'
ACTION='BORG_BACKUP'
borg create --compression lz4 $BORG_BACKUP_FOLDER::'{now}' $BACKUP_FOLDER                  
checkRet $SERVER $ACTION

ACTION='BORG_PRUNE'
borg prune --list --keep-daily 7 $BORG_BACKUP_FOLDER	 	
checkRet $SERVER $ACTION

	# UMOUNT_SSHFS
	SERVER='SRV-NAT'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-nat"
	checkRet $SERVER $ACTION
	SERVER='SRV-DHCP'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-dhcp"
	checkRet $SERVER $ACTION
	SERVER='SRV-MGNT'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-mgnt"
	checkRet $SERVER $ACTION
	SERVER='SRV-WORKER-1'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-worker-1"
	checkRet $SERVER $ACTION
	SERVER='SRV-WORKER-2'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-worker-2"
	checkRet $SERVER $ACTION
	SERVER='SRV-DB-MASTER'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-db-master"
	checkRet $SERVER $ACTION
	SERVER='SRV-DB-SLAVE'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-db-slave"
	checkRet $SERVER $ACTION
	SERVER='SRV-PREPROD'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-preprod"
	checkRet $SERVER $ACTION
	SERVER='SRV-MAIL'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-mail"
	checkRet $SERVER $ACTION
	SERVER='SRV-SERVICES'
	ACTION='UMOUNT_SSHFS'
	fusermount -u $BACKUP_FOLDER"srv-services"
	checkRet $SERVER $ACTION


	echo "====STOP BACKUP====" >> $LOG_FILE
	echo "======================================" >> $LOG_FILE
	echo "======================================" >> $LOG_FILE

	exit 0


