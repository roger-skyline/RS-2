# How to connect to XMPP server

In this tutorial, because of macOs environnement, we are going to use the XMMP client [Adium](https://adiumx.cachefly.net/Adium_1.5.10.4.dmg).

## Step 1 : Configure your JID

In this step, you will define the user who'll be logged on Jabber.

Use `<LDAP_USERNAME>@xmpp.slash16.local` as Jabber ID.  

Use `<LDAP_USER_PASSWD>` as password.    

<p align="center">
  <img src="assets/step_1.png" height="400" width="400"/>
</p>

## Step 2 : Configure server informations

Use `10.17.1.125` as server to reach and specify the port `5222`  

Check the `Require TLS/SSL` option.

<p align="center">
  <img src="assets/step_2.png" height="400" width="400"/>
</p>

## Step 3 : Log in

Once logged in, you'll need to accept the certificate before start conversations.

<p align="center">
  <img src="assets/step_3.png" height="400" width="400"/>
</p>

## Step 3 : Enjoy !

Now you can chat with the world, throwback in 90's

<p align="center">
  <img src="assets/step_4.png" height="400" width="400"/>
</p>
