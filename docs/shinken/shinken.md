# Shinken

Shinken is an open source computer system and network monitoring software application compatible with Nagios. 
It watches hosts and services, gathers performance data and alerts users when error conditions occur
and again when the conditions clear. 


## Configure a host 

Shinken will run on the whole hosts configured inside the `/etc/shinken/hosts/` the checks you desire to be
monitored by Shinken. This is how a host is configured:

    define host{
	    use                 		generic-host,<SERVICE>,<YOU>,<WANT>,<ON>,<HOST>
	    contact_groups      		<GROUPS>    <--- Groups that will be notify by Shinken alerts 
	    host_name           		<HOST_NAME> <--- The hostname of your machine.
	    address             		<xx.x.x.xx> <--- The adress of yout machine.
    }
