Mail Service 
-----

```
Postfix 3.1.8
Dovecot 2.2.27 (c0f36b0)
SpamAssassin version 3.4.2
  running on Perl version 5.24.1
```


## SMTPS
SMTPS is handle by postfix on port 587 through TLS.
The postfix core config file is located in `/etc/postfix/master.cf`.
It allow the communication between posfix-dovecot / postfix-spamassassin.
All the configuration specific to SMTPS are done in `/etc/postfix/main.cf`.
smtp / smtpd / tls / ldap / relay restrictions / 
ldap conf files are in `/etc/ldap/ldap-*.cf`


## POP3S - IMAPS
They are running with dovecot, the core config file is located in `/etc/dovecot/dovecot.conf`.
This file include all files in `/etc/dovecot/conf.d/`.
 - 10-auth.conf -> auth-ldap.conf.ext  ; ldap auth 
 - 10-master.conf  ;  handle port / protocol and the listener 
 - 10-mail.conf  ;  type of mailbox and his location
 - 10-ssl.conf  ;  tls

 
