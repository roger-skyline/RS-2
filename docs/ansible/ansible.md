Automatisation with Ansible
------

Debops permit to have a various of roles available.
And recommend to have a proper ansible directory tree:

```
.
├── inventory
│   ├── group_vars
│   │   ├── all
│   │   │   ├── apt_install.yml
│   │   │   ├── etckeeper.yml
│   │   │   ├── network-ini.yml
│   │   │   ├── root_accounts.yml
│   │   │   ├── sudo.yml
│   │   │   └── users.yml
│   │   ├── mongodb
│   │   └── worker
│   │       ├── nodejs_project.yml
│   │       └── users.yml
│   ├── hosts
│   └── host_vars
│       ├── srv-db-master
│       │   └── security.yml
│       ├── srv-db-slave
│       │   └── security.yml
│       ├── srv-dhcp
│       │   └── security.yml
│       ├── srv-mail
│       │   └── security.yml
│       ├── srv-mgnt
│       │   ├── apt_install.yml
│       │   ├── pip.yml
│       │   ├── python.yml
│       │   ├── resources.yml
│       │   ├── security.yml
│       │   └── users.yml
│       ├── srv-nat
│       │   ├── authorized_keys.yml
│       │   ├── debops.yml
│       │   ├── security.yml
│       │   └── users.yml
│       ├── srv-preprod
│       │   ├── nodejs_project.yml
│       │   ├── security.yml
│       │   └── users.yml
│       ├── srv-services
│       │   └── security.yml
│       ├── srv-worker-1
│       │   ├── network.yml
│       │   └── security.yml
│       └── srv-worker-2
│           └── security.yml
├── playbooks
│   ├── ansible_init.yml
│   ├── cert.yml
│   ├── change_hostname.yml
│   ├── connection.yml
│   ├── custom.yml
│   ├── gate_init.yml
│   ├── init.yml
│   ├── master.yml
│   ├── nodejs_project.yml
│   ├── nodejs.yml
│   ├── security-custom.yml
│   ├── shinken_hosts.yml
│   ├── shinken_install.yml
│   ├── ssmtp.yml
│   └── upgrade.yml
├── resources
│   ├── admin
│   │   ├── id_admin
│   │   └── id_admin.pub
│   ├── ansible
│   │   ├── id_ansible
│   │   └── id_ansible.pub
│   ├── certs
│   │   └── ca-certificates.crt
│   ├── deploy
│   │   ├── id_deploy
│   │   └── id_deploy.pub
│   ├── etckeeper
│   │   ├── id_etckeeper
│   │   └── id_etckeeper.pub
│   └── worker
│       ├── id_github_rsa
│       └── id_github_rsa.pub
├── roles
│   ├── ansible-network-init -> ../../../ansible-network-init
│   ├── ansible-network-interfaces -> ../../../ansible-network-interfaces
│   ├── ansible-role-etckeeper -> ../../../ansible-role-etckeeper
│   ├── ansible-role-firewall -> ../../../ansible-role-firewall
│   ├── ansible-role-nodejs -> ../../../ansible-role-nodejs
│   ├── ansible-role-nodejs-project -> ../../../ansible-role-nodejs-project
│   ├── ansible-role-pip -> ../../../ansible-role-pip
│   ├── ansible-role-pm2 -> ../../../ansible-role-pm2
│   └── ansible-role-security -> ../../../ansible-role-security
└── secret
    └── credentials
        ├── srv-db-master
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-db-slave
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-dhcp
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-init
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-mail
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-mgnt
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-nat
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-preprod
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       ├── ansible
        │       │   └── password
        │       ├── deploy
        │       │   └── password
        │       ├── preprod
        │       │   └── password
        │       └── worker
        │           └── password
        ├── srv-service
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-services
        │   └── users
        │       ├── admin
        │       │   └── password
        │       └── ansible
        │           └── password
        ├── srv-worker-1
        │   ├── root_account
        │   │   └── password
        │   └── users
        │       ├── admin
        │       │   └── password
        │       ├── ansible
        │       │   └── password
        │       └── worker
        │           └── password
        └── srv-worker-2
            ├── root_account
            │   └── password
            └── users
                ├── admin
                │   └── password
                ├── ansible
                │   └── password
                └── worker
                    └── password

100 directories, 96 files
```
