## User / Group

admin: gid > 1000, uid > 1000
ansible: gid > 1000, uid > 1000

### Global configuration
On each machine a sudoeur user will be created to execute ansible task/
This user will be also used is case of LDAP failed to respond

### Specific configuration
There is no specific configuration because the LDAP must serve every machine and provide the user's `/home` directory everywhere

### Ansible
An ansible user must be sudoer ans run command with sudo without password to perform his tasks.
Private and public key of ansible user will be on your local computer and on the ansible machine. Only public key will be on each host.
