Commands list for playbooks
------
(You need to source python env file before running any commands)

## Specific playbook
### [nodejs_project.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/nodejs_project.yml)

I. tags available:
Update npm repositories
- `npm_update`
- `git_update`
- `pm2_restart`
- `pm2_start`

II. variables available:
Hostname (or group) to apply playbook
- `host`
Can be : `srv-worker-1` or `worker`

#### Prototype:
`debops playbooks/nodejs_project.yml --extra-vars "host=<hostname_or_group>" -t <tag_name>`

#### Example:
Will update worker's npm repositories
`debops playbooks/nodejs_project.yml --extra-vars "host=worker" -t npm_update`

## Role playbooks
Command to execute playbook:
`debops playbooks/<playbook_name>`

VM initialisation, network, system packets repository, users, security ...
### [init.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/init.yml)
Tags are use to run only wanted roles.
All available tags are listed in [init.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/init.yml).

Available variables are specified in eachs `default/main.yml` file of roles.
