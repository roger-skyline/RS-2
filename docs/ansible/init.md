## First initials steps

### Enter in the gate
Use root user to connect to the gateway VM and copy local ssh public key to `/root/authorized_keys` file, configure sshd, users and firewall
`debops --ask-pass playbooks/gate_init.yml`

*ansible_port* and *ansible_user* must be change in inventory if `role::security` has rewrite them

### Install Debops / ansible
We need to install Debops playbook and ansible
`debops playbooks/ansible_init.yml`

### Deployment
Now you can access to the vm where ansible is install.
In your `inventory/hosts` file, `ansible_host`, `ansible_ssh_private_key_file`, `ansible_user` and `ansible_user` must be define for each hosts

If your hosts are brand new (only root user without ssh key), you must connect throw it with ssh root password. So `ansible_user` must be changed temporary.
`sshpass` package will be used.

After the first run succed, you can reset `ansible_user` var to you ansible user who execute remote commands.
