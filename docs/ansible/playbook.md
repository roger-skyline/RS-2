Playbooks inventory
------

### Init
Configure gateway machine   
[gate_init.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/gate_init.yml)

Configure network interface   
[connection.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/connection.yml)

Upgrade distribution to stretch   
[upgrade.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/upgrade.yml)

Change hostname   
[change_hostname.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/change_hostname.yml)

### Shinken
Deploy shiken conf   
[shinken_hosts.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/shinken_hosts.yml)

Install shiken   
[shinken_install.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/shinken_install.yml)

### Apps
Apply specific commands on workers
[nodejs_project.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/nodejs_project.yml)

Initialise and launch workers
[nodejs.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/nodejs.yml)

### System
Install ansible   
[ansible_init.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/ansible_init.yml)

Install ssmtp package (mail client)   
[ssmtp.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/ssmtp.yml)

Master playbook to call other playbooks   
[master.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/master.yml)

Deploy ldap public certificate   
[cert.yml](https://github.com/Roger-skyline/Debops/blob/master/ansible/playbooks/cert.yml)
