## Playbook roles

`role::authorized_keys`
------
Define resource dir path
`resources__src: '{{ inventory_dir | realpath + "/../resources/" }}'`

Path for ssh key auth definition
`authorized_keys__path: /root/.ssh`   

Disable key from /etc/ssh/
`authorized_keys__system: false`    

List of auth key to put in `/root/.ssh/authorized_keys`   
```
authorized_keys__list:
  - name: 'authorized_keys'
	mode: 600
    sshkeys: [ '{{ lookup("file", resources__src + "admin/ssh_key.pub") }}'
```   

`role::root_account`
------

Update root password   
`root_account__password_update: true` 

Do not generate root ssh key   
`root_account__generate_ssh_key: false`

Password length   
`root_account__password_length: '16'`
Disable configuration for subordinate uid for root

`root_account__subuid_enabled: false`
Root password path   
```
root_account__password: '{{ lookup("password", secret
    + "/credentials/" + inventory_hostname
	+ "/root_account/password encrypt=sha512_crypt length="
	+ root_account__password_length) }}'
```


`role::sudo`
------
Enable use of sudo   
`sudo__enabled: true`

Sudo configuration for specific user   
`sudo__sudoers:`

`role::users`
------
Create user with uid > 1000
`users__default_system: false`

Default shell
`users__default_shell: '/bin/bash'`

List of user to create   
For each user, we givea `name`, `group`, `groups`, `shell`, `password` and `sshkeys`
We must also precise if user pasword must be updated on creation with `update_password`
`users__account:`

`role::sshd`
------
Need to set another port and user to connect throw ssh   
`security_ssh_port: `   
`security_ssh_new_user: "user1"`

Only ssh key are accepted   
`security_ssh_password_authentication: "no"`

Root account is disabled   
`security_ssh_permit_root_login: "no"`

Allow specific users to connect throw ssh   
`security_ssh_allow_users:  [ 'user1', 'user2' ]`

We do not want default instaled fail2ban service   
`security_fail2ban_enabled: false`

`role::firewall`
------
Open some port   
`firewall_allowed_tcp_ports: []`

Allow input ping   
`firewall_allowed_icmp_packets: true`

Allow some IPv4   
```
firewall_allowed_tcp_by_ipv4:
  - { ipv4: 127.0.0.1, service: "{{ security_ssh_port }}", state: NEW }
```

- `role::ansible`
[](Basic configuration)

`role::debops`
------
`debops__project_git_repo: 'https://'`   
`debops__install_systemwide: false`   
`debops__data_path: '{{ ansible_local.root.share + "/debops" }}'`   
`debops__project_name: 'Debops'`

`role::etckeeper`
------
Versioning is done with our internal gogs application (based on git)
Every vm repositoy must be created in PRIVATE, and public key of etc keeper need to be added to his gogs account
`etckeeper__vcs: 'git'`
User who will run etckeeper
`etckeeper__vcs_user: 'etckeeper'`
`etckeeper__vcs_email: `
Need to install cron to launch etckeeper everyday
`etckeeper__package: [ "cron" ]`
