## Core Structure
### DNS
- Install DNS server in a VM
- Add IPv4 of DNS server in every new VM
- Define DNS in DHCP server

### DHCP
- Install DHCP server
- Change network conf to DHCP in every new VM
- Add DHCP lease for every VM in config

### NAT
- Active ipv4 forwarding
- Add gateway server IPv4 to every VM
- Script port forwarding for every infrastructure's services

### LADP
- install LDAP server
- install LADP client on every VM
- Versionnning web app must use this server
